/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

/**
 *
 * @author coloque acá sus nombres completos
 * @param <T> un tipo de dato objeto
 */
public class ListaS<T extends Comparable> {

    private Nodo<T> cabeza;
    private int size;

    public ListaS() {
        this.cabeza = null;
    }

    public void insertarInicio(T objeto) {
        Nodo<T> nuevo = new Nodo(objeto, this.cabeza);
        this.cabeza = nuevo;
        size++;
    }
    
    public T get(int i){
        return this.getPos(i).getInfo();
    }
    
    public void set(int i, T elemento){
        this.getPos(i).setInfo(elemento);
    }
    
    public T remove(int i){
        this.validarPos(i);
        //Caso 1: Borrar la cabeza
      
        Nodo<T> b = null;
        Nodo<T> anterior;
        if(i == 0){
            b = this.cabeza;
            this.cabeza = b.getSig();
        }else{
            anterior = this.getPos(i-1);
            b = anterior.getSig();
            anterior.setSig(b.getSig());
        }
        b.setSig(null);
        this.size--;
        return b.getInfo();
    }

    public boolean esVacio() {
        return this.cabeza == null;
    }

    public int getSize() {
        return size;

    }

    public String toString() {
        String msg = "cab<>";
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            msg += i.getInfo().toString() + "->";
        }
        return msg + "null";
    }

    public boolean contains(T objeto) {
        if (objeto == null || this.esVacio()) {
            throw new RuntimeException("no puede buscar un elemento");
        }
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            if (i.getInfo().equals(objeto)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        this.cabeza = null;
        this.size = 0;
    }

    public void insertarFin(T objeto) {
        if (this.esVacio()) {
            this.insertarInicio(objeto);
        } else {
            Nodo<T> nuevo = new Nodo(objeto, null);
            Nodo<T> ultimo = this.getPos(this.size - 1);
            ultimo.setSig(nuevo);
            size++;
        }
    }

    

    private Nodo<T> getPos(int i) {
        this.validarPos(i);
        //Referenciar, no crear
        Nodo<T> pos = this.cabeza;
        for (int j = 0; j < i; j++) {
            pos = pos.getSig();
        }
        return pos;
    }

    private void validarPos(int i) {
        if (this.esVacio() || i < 0 || i >= this.size) {
            throw new RuntimeException("Indice: " + i + " fuera de Rango");
        }
    }
    
    /**
     * Entendiendo 'ascendente' como, desde el mayor, hasta el menor, la lista
     * debe tener una lógica tal que: ListaS: 10 -> 1 ListaS: Z - a ListaS: 10
     * -> 9 -> 8 -> 6 -> 7
     */
    public void deleteError() {
        try {
            for (Nodo<T> temp = cabeza; temp.getSig() != null; temp = temp.getSig()) {
                /**
                 * Si el elemento dentro del nodo 'temp' es menor que el
                 * elemento dentro del nodo que le sigue, ese es el error, por
                 * lo tanto, se elimina, reapuntando
                 */
                while (temp.getInfo().compareTo(temp.getSig().getInfo()) > 0) {

                    Nodo<T> newMinor = temp.getSig().getSig();

                    //Favor descomentar estas líneas para ver a detalle el progreso del código
                    /**
                     * System.out.println("La info del nodo evaluado es: " +
                     * temp.getInfo());
                     *
                     * System.out.println("Su nodo siguiente es: " +
                     * temp.getSig().getInfo());
                     *
                     * System.out.println("El nuevo nodo al que se va a apuntar
                     * es: " + newMinor.getInfo());
                     */
                    temp.setSig(newMinor);

                    //Favor descomentar estas líneas para ver a detalle el progreso del código
                    /**
                     * System.out.println("El nuevo nodo siguiente es: " +
                     * temp.getSig().getInfo());
                     */
                }
            }
        } catch (NullPointerException e) {
            throw new RuntimeException("""
                               ERROR 'PANECILLO':
                               Alguno de las direcciones a las que apunta el nodo del bucle es nula 'Null',
                               o la lista esta vacia...""");
        } catch (Exception e) {
            throw new RuntimeException("""
                               ERROR 'CUPCACKE':
                               Se ha ejecutado un error no identificado""");
        }
    }

}
