/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

/**
 *
 * @author coloque acá sus nombres completos
 */
/**
     * Entendiendo 'ascendente' como, desde el mayor, hasta el menor, la lista
     * debe tener una lógica tal que: ListaS: 10 -> 1 ListaS: Z - a ListaS: 10
     * -> 9 -> 8 -> 6 -> 7
     */
    public void deleteError() {
        try {
            for (Nodo<T> temp = cabeza; temp.getSig() != null; temp = temp.getSig()) {
                /**
                 * Si el elemento dentro del nodo 'temp' es menor que el
                 * elemento dentro del nodo que le sigue, ese es el error, por
                 * lo tanto, se elimina, reapuntando
                 */
                while (temp.getInfo().compareTo(temp.getSig().getInfo()) > 0) {

                    Nodo<T> newMinor = temp.getSig().getSig();

                    //Favor descomentar estas líneas para ver a detalle el progreso del código
                    /**
                     * System.out.println("La info del nodo evaluado es: " +
                     * temp.getInfo());
                     *
                     * System.out.println("Su nodo siguiente es: " +
                     * temp.getSig().getInfo());
                     *
                     * System.out.println("El nuevo nodo al que se va a apuntar
                     * es: " + newMinor.getInfo());
                     */
                    temp.setSig(newMinor);

                    //Favor descomentar estas líneas para ver a detalle el progreso del código
                    /**
                     * System.out.println("El nuevo nodo siguiente es: " +
                     * temp.getSig().getInfo());
                     */
                }
            }
        } catch (NullPointerException e) {
            throw new RuntimeException("""
                               ERROR 'PANECILLO':
                               Alguno de las direcciones a las que apunta el nodo del bucle es nula 'Null',
                               o la lista esta vacia...""");
        } catch (Exception e) {
            throw new RuntimeException("""
                               ERROR 'CUPCACKE':
                               Se ha ejecutado un error no identificado""");
        }
    }
